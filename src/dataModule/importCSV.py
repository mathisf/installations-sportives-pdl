import csv
from bd import *

def importData():
    # Open the csv files read-only
    activites = open("../../csv/activites.csv", "r")
    equipements = open("../../csv/equipements.csv", "r")
    installations = open("../../csv/installations.csv", "r")

    # Read the CSV files
    csvReaderA = csv.reader(activites)
    csvReaderE = csv.reader(equipements)
    csvReaderI = csv.reader(installations)

    # Create tables if they are not already created
    bd = BD()

    if(not bd.tableExist("installations")):
        bd.execute("CREATE TABLE installations (id int(11) NOT NULL, nom varchar(30) NOT NULL, commune varchar(30) NOT NULL, cp int(5) NOT NULL, lieudit varchar(30), numvoie int(3), nomvoie varchar(30), longitude double, lattitude double,  PRIMARY KEY (id))")
        sql = """INSERT IGNORE INTO installations VALUES (%s,  %s,  %s,  %s,  %s,  %s,  %s, %s, %s)"""
        for row in csvReaderI:
             bd.execute(sql, (row[1], row[0], row[2], row[4], row[5], row[6], row[7], 0.0, 0.0))

    if(not bd.tableExist("equipements")):
        bd.execute("CREATE TABLE equipements (id int(11) NOT NULL, nom varchar(40) NOT NULL, idInstallation int(11) NOT NULL, PRIMARY KEY (id), FOREIGN KEY (idInstallation) REFERENCES installations(id))")
        sql = """INSERT IGNORE INTO equipements VALUES (%s,  %s,  %s)"""
        for row in csvReaderE:
            bd.execute(sql, (row[4], row[5], row[2]))

    if(not bd.tableExist("activites")):
        bd.execute("CREATE TABLE activites (id int(11) NOT NULL, nom varchar(40) NOT NULL, PRIMARY KEY (id))")
        sql = """INSERT IGNORE INTO activites VALUES (%s,  %s)"""
        bd.execute(sql, (row[4], row[5]))

    if(not bd.tableExist("equipements_activites")):
        bd.execute("CREATE TABLE equipements_activites (idEquipement int(11) NOT NULL, idActivite int(11) NOT NULL, FOREIGN KEY idActivite REFERENCES activites(id), FOREIGN KEY idEquipement REFERENCES equipements(id))")
        sql = """INSERT IGNORE INTO equipements_activites VALUES (%s,  %s)"""
        for row in csvReaderA:
            if(row[4] != 0):
                bd.execute(sql, (row[2], row[4]))
