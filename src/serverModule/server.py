from dataModule.bd import *
from lib.bottle import *
from urllib import request as req
from urllib.parse import *

@route('/recherche')
def recherche():
	commune = request.query.commune
	actId = request.query.actId
	bd = BD()

	if(commune!="all" and actId!="all"):
		data = bd.getRows("SELECT e.nom, i.commune, i.cp, i.lieudit,  i.numvoie, i.nomvoie, i.latitude, i.longitude from installations i, equipements e, equipement_activite eqact, activites a where i.id=e.id and e.id=eqact.idEquipement and eqact.idActivite=a.id and a.id=\""+actId+"\" and i.commune=\""+commune+"\";")
		resultat = template("", data)
	if(commune=="all" and actId!="all"):
		data = bd.getRows("SELECT e.nom, i.commune, i.cp, i.lieudit, i.numvoie, i.nomvoie, i.latitude, i.longitude from installations i, equipements e, equipement_activite eqact, activites a where i.id=e.id and e.id=eqact.idEquipement and eqact.idActivite=a.id and a.id=\""+actId+"\";")
		resultat = template("", data)
	if(commune!="all" and actId=="all"):
		data = bd.getRows("SELECT a.nom, e.nom, i.commune, i.cp, i.numvoie, i.nomvoie, i.latitude, i.longitude from installations i, equipements e, equipement_activite eqact, activites a where i.id=e.id and e.id=eqact.idEquipement and eqact.idActivite=a.id and i.commune=\""+commune+"\";")
		resultat = template("", data)
	if(commune=="all" and actId=="all"):
		data = bd.getRows("SELECT a.nom, e.nom, i.commune, i.cp, i.numvoie, i.nomvoie, i.latitude, i.longitude from installations i, equipements e, equipement_activite eqact, activites a where i.id=e.id and e.id=eqact.idEquipement and eqact.idActivite=a.id;")
		resultat = template("", data)
	return resultat

@route('/')
def installation():
	bd = BD()
	data = bd.getRows("SELECT id, nom from activites order by nom asc;")
	data2 = bd.getRows("SELECT DISTINCT commune from installations order by commune asc;")
	liste = [data, data2]
	resultat = template("", rows=liste)
	return resultat

@route('/static/img/:filename')
def serve_static(filename):
	return static_file(filename, root="")

run(host='localhost', port=9999, debug=True)
